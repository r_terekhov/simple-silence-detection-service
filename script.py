import requests
import os,os.path, time
from datetime import datetime
from pydub import AudioSegment,silence

def telegram_bot_sendtext(bot_message):

   bot_token = '1944098392:AAHMBVrYLiOzwVxWcH9JPI7mqNKqSmNknhI'
   bot_chatID = '-582707794'
   send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + bot_message

   response = requests.get(send_text)

   return response.json()

def minutes_between(d1, d2):
    return abs((d2 - d1).total_seconds() /60.0)
    
def creation_date(file_path):
    t = os.path.getmtime(file_path)
    return datetime.fromtimestamp(t)

def detect_silence(filepath,silence_min_len,max_silence_which_means_error,filename):
  total_silence = 0
  silence_result = 0
  myaudio = intro = AudioSegment.from_mp3(filepath)
  dBFS=myaudio.dBFS
  silence_result = silence.detect_silence(myaudio, min_silence_len=silence_min_len, silence_thresh=dBFS-16)
  silence_result = [((start/1000),(stop/1000)) for start,stop in silence_result] #in sec
    
  for silence_item in silence_result:
    total_silence+= silence_item[1] - silence_item[0]
    
  if (total_silence >=max_silence_which_means_error):
    print(filename, "SILENCE DETECTED! ", "SILENCE_LENGTH IS", total_silence)
    print("DETAILS:")
    print(silence_result)
    text = 'SILENCE DETECTED! callTask is {}, silence total length is {}, details is {}'.format(filename, total_silence, silence_result)
    telegram_bot_sendtext(text) 
  return total_silence

silence_min_len = 5000  #5 sec
max_silence_which_means_error = 25
counter = 0
silence_counter = 0
now = datetime.now()
interval_in_minutes = 0.5


while True:
  print("SCRIPT STARTED")
  try:
    for root, dirs, files in os.walk('/home/roman/Documents/vkr'):
      for file in files:
        if file.endswith('.wav'):
          counter+=1
          path = os.path.join(root, file)
          if minutes_between(creation_date(path), now) <= interval_in_minutes:
            if (detect_silence(path,silence_min_len,max_silence_which_means_error,file) >=max_silence_which_means_error):
              silence_counter+=1
  except FileNotFoundError:
    print('error blyat')
    pass
  print("% of files with silence is", silence_counter / (counter +1) * 100, "%")           
  print("SCRIPT_ENDED")
