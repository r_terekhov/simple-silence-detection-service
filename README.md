# simple-silence-detection-service

# Описание 

 Сервис проходится по записям звонков по кампании и определяет молчание, если оно есть, то пишет в телеграм.

# Установка:

  1. install python
  2. install pip
  3. pip install pydub
  4. apt install ffmpeg


# Использование

  1. Перед началом прозвона поменять кампанию в файле script
  2. systemctl daemon-reload
  3. systemctl restart silence-detection.service
  4. systemctl status silence-detection.service
  5. После окончания прозвона обязательно остановить (systemctl stop silence-detection.service)

